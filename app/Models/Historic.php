<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

class Historic extends Model
{
    protected $fillable = [
        'type',
        'amount',
        'total_before',
        'total_after',
        'user_id_transaction',
        'date'
    ];

    public function type($type = null)
    {
        $types = [
            'I' =>  'Entrada',
            'O' =>  'Saque',
            'T' =>  'Transferência'
        ];

        if (!$type) {

            return $types;
        }

        if ($this->user_id_transaction !== null && $type == 'I') {

            return 'Recebido';
        } else {

            return $types[$type];
        }
    }

    public function scopeUserAuth($query)
    {
        return $query->where('user_id', auth()->user()->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function userSender()
    {
        return $this->belongsTo(User::class, 'user_id_transaction');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/y');
    }

    public function search(array $data, $totalPaginate)
    {
        $historics = $this->where(function ($query) use ($data) {
            if (isset($data['id'])) {
                $query->where('id', $data['id']);
            }
            if (isset($data['date'])) {
                $query->where('date', $data['date']);
            }
            if (isset($data['type'])) {
                $query->where('type', $data['type']);
            }
        })
            // ->where('user_id', auth()->user()->id)
            ->userAuth()
            ->with(['userSender'])
            ->paginate($totalPaginate);
            //->toSql();dd($historics);

        return $historics;
    }
}
