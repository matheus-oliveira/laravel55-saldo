@extends('site.layouts.app')

@section('title','Meu Perfil')

@section('content')
<h1>Meu Perfil</h1>

@if(session('success'))
<div class="alert alert-success" role="alert">
    {{ session('success') }}
</div>
@endif
@if(session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif

<form action="{{ route('profile.update')}}" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" value="{{auth()->user()->name}}" class="form-control" name="name" placeholder="Nome">
    </div>
    <div class="form-group">
        <label for="email">email</label>
        <input type="email" value="{{auth()->user()->email}}" class="form-control" name="email" placeholder="E-mail">
    </div>
    <div class="form-group">
        <label for="password">Senha</label>
        <input type="password" class="form-control" name="password" placeholder="Senha">
    </div>
    <div class="form-group">
        @if(auth()->user()->image !=null )
        <img class="rounded-circle" height="50px" src="{{ url('storage/users/'.auth()->user()->image) }}" alt="">
        @endif
         @if(auth()->user()->image ==null )
        <img class="rounded-circle" height="50px" src="{{ url('storage/users/default.jpg') }}" alt="">
        @endif
        <img src="" alt="">

        <label for="image">Imagem:</label>
        <input type="file" class="form-control" name="image">
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-info">Atualizar Perfil</button>
    </div>
</form>

@endsection