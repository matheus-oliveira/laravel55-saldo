@extends('adminlte::page')

@section('title', 'Nova recarga ')

@section('content_header')
<h1>Fazer recarga</h1>
<ol class="breadcrumb">
    <li><a href="">Dashboard</a></li>
    <li><a href="">Saldo</a></li>
    <li><a href="">Transferir saldo</a></li>
</ol>
@stop

@section('content')
<div class="box">
    <div class="box-header">
        <h1>Confirmar transferencia saldo</h1>
    </div>
    <div class="box-body">
        @include('admin.includes.alerts')
        <p><strong>Recebedor: </strong>{{ $sender->name }}</p>
        <p><strong>Saldo disponivel: </strong>{{ number_format($balance->amount ,2 ,',','.') }}</p>
        <form method="POST" action="{{ route('transfer.store') }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="hidden" name="sender_id" id="" value="{{ $sender->id }}" class="form-control">
                <input type="text" name="value" id="" class="form-control" placeholder="Valor:" aria-describedby="helpId">
            </div>
            <div class="form-group">

                <button type="submit" class="btn btn-success">Proxima etapa</button>

            </div>
        </form>
    </div>
</div>
@stop