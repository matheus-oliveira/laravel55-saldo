@extends('adminlte::page')

@section('title', 'Nova recarga ')

@section('content_header')
<h1>Fazer recarga</h1>
<ol class="breadcrumb">
    <li><a href="">Dashboard</a></li>
    <li><a href="">Saldo</a></li>
    <li><a href="">Depositar</a></li>
</ol>
@stop

@section('content')
<div class="box">
    <div class="box-header">
        <h1>Fazer recarga</h1>
    </div>
    <div class="box-body">
        @include('admin.includes.alerts')

        <form method="POST" action="{{ route('balance.deposit') }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" name="value" id="" class="form-control" placeholder="Valor recarga" aria-describedby="helpId">
            </div>
            <div class="form-group">

                <button type="submit" class="btn btn-success">Recaregar</button>

            </div>
        </form>
    </div>
</div>
@stop