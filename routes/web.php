<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// $this->group([],function(){

// });

$this->group(['middleware' => ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get  ('/', 'AdminController@index')->name('admin.home');

    Route::any  ('historic-search', 'BalanceController@searchHistoric')->name('historic.search');
    // o parametro any envia a requsição via POST e GET
    Route::get  ('historic', 'BalanceController@historic')->name('admin.historic');

    Route::post ('balance/transfer', 'BalanceController@transferStore')->name('transfer.store');
    Route::post ('balance/transfer/confirm-transfer', 'BalanceController@confirmTrasnfer')->name('confirm.transfer');
    Route::get  ('balance/transfer', 'BalanceController@transfer')->name('balance.transfer');

    Route::post ('balance/withdraw', 'BalanceController@withdrawStore')->name('withdraw.store');
    Route::get  ('balance/withdraw', 'BalanceController@withdraw')->name('balance.withdraw');

    Route::post ('balance/deposit', 'BalanceController@depositStore')->name('deposit.store');
    Route::get  ('balance/deposit', 'BalanceController@deposit')->name('balance.deposit');
    //so pode ter a mesma rota pq uma usa o metodo post e a outra o get
    Route::get  ('balance', 'BalanceController@index')->name('admin.balance');
});

Route::post ('atualizar-perfil', 'Admin\UserController@profileUpdate')->name('profile.update')->middleware('auth');;
Route::get  ('meu-perfil', 'Admin\UserController@profile')->name('profile')->middleware('auth');

Route::get ('/', 'Site\SiteController@index')->name('home');
//or this structure
// $this->get('/', 'Site\SiteController@index');


Auth::routes();
