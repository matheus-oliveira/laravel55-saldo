<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'matheus Oliveira',
            'email'     => 'moguedes98@gmail.com',
            'password'  => bcrypt('123456789'),
        ]);
        User::create([
            'name'      => 'Marcela Victoria',
            'email'     => 'marcela.oliveirasilva16@gmail.com ',
            'password'  => bcrypt('123456789'),
        ]);
        
    }
}
